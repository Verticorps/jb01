JIRA Basics Workshop 01
=====================

Files needed to complete JB01 workshop.


Requirements
------------

You will need the latest versions of the following applications, which all run under Linux, macOS, and Windows:

* [Git](https://git-scm.com/downloads) or [SourceTree](https://www.sourcetreeapp.com/)
* [VirtualBox 5.1.26 or higher](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant 1.9.3 or higher](https://www.vagrantup.com/downloads.html)


Use
------------

Once VirtualBox & Vagrant are installed, simply run the following commands:

```
git clone https://bitbucket.org/Verticorps/jb01.git
cd jb01
vagrant up
```

Depending on network and computer speed, you should have a new test VM configured in 10-15 minutes.

Once Vagrant has successfully built your jb01 test environment, you may log in with:

```
vagrant ssh
```

### Note
If you get an error that looks similar to the following, there are a number of potential solutions:
> Vagrant cannot forward the specified ports on this VM, since they would collide with some other application that is already listening on these ports. The forwarded port to 8000 is already in use on the host machine.

1. You may have a firewall and/or antivirus that is protecting the needed ports. Temporarily disable them.
2. If you're comfortable configuring firewall rules, create a rule to allow VirtuaBox & Vagrant to access ports 8080, 8443, and 9001.
3. You may already have another VM running with these ports enabled. Shut off any other VirtualBox and VMWare VMs.

##### Feel The Power!
![Vagrant - Now you're playing with power!](https://bytebucket.org/Verticorps/jb01/raw/561ec2e8c8edeaacc2bc5b1e0be78d77ab63e245/power.jpg)
